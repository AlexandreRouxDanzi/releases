# Please welcome /e/OS 1.17! :rocket:

We are proud to deliver the /e/OS 1.17. Enjoy all the new features and improvements it embeds!

## ✨ We embedded some improvements!

Community devices
-  Some community devices were missing images for flashing: fix and update documentation   

Updater
-  Updater crashes prevention    
-  Translation improvements

microG 
-  We moved to Stadia map tiler backend

App Lounge 
-  The color of the banner now adapts to the theme in use    


## 🕙 Software updates

Browser update
- Browser update including security patches

microG
- microG update with upstream    

## 🕙 Software updates

➕ **LineageOS 19.1** last bug fixes and security updates has been merged ([list](https://review.lineageos.org/q/branch:lineage-19.1+status:merged+after:%222023-09-20+21:31:00+%252B0200%22+before:%222023-10-24+12:33:00+%252B0200%22))


FP5
- Upgrade firmware to FP5.TT3P.A.112.20231016   

- Firmware update for S7/S7 edge and S8/S8+

Tearcube 2e (2020&2021)
-  Upgrade firmware to SP1A.210812.016    

## 🐛 Bug fixes

Browser
-  Bookmark folders in Browser are now functional
-  Inapp browser integration is now working instead of opening a full browser instance

Message
-  SMS messages are now sent or received SMS messages only once as expected 

Settings
-  Subsettings in Settings for A13 do not crash anymore
- The notification light option is not available in Settings when there is no notification light on the phone

Update
- Long text strings are now perfectly readable in the Updater
-  OTA server now only lists upgrades which are suitable for the phone


App Lounge
-  PWA label is again displayed in search results
-  AppLounge opens F-Droid links properly
-  Search results are now exhaustive whatever the source
-  Silent notifications for App Lounge now follow our palette     
-  App lounge now warns user when space is insufficient to handle updates    

eDrive
-  eDrive does not get stuck while syncing anymore


Teracube 2e 2020 and 2021
-  Fix double tap to wakeup    
-  It's now possible to send SMS with Bouygues carrier on Android S with Teracube 2e
-  All rear lens are now available in Camera for Teracube 2e 


FP4
- Camera lens are now all available in Camera for Fairphone 4
-  Pixel 4a, Pixel 6a and Fairphone 4 on Android 13 do not reboot randomly anymore   


Community devices
-  Xiaomi variants are now built for A12
 

## ⚠ Security fixes

This /e/OS v1.17 includes the [Android security patches](https://source.android.com/security/bulletin/2023-10-01) available until October.
